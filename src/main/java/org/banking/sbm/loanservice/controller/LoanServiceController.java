package org.banking.sbm.loanservice.controller;

import org.banking.sbm.loanservice.model.AppliedLoan;
import org.banking.sbm.loanservice.model.LoanRequest;
import org.banking.sbm.loanservice.service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class LoanServiceController {

    @Autowired
    @Qualifier("loanService")
    private LoanService loanService;

   
    @PostMapping("/loans")
    public ResponseEntity<Object> applyForLoan(@RequestBody LoanRequest lr) {
        return loanService.loanEntry(lr);
    }

    @PostMapping("/customer/{customerid}/loans/{loanid}")
    public ResponseEntity<Object> appliedLoanEntry(@RequestBody AppliedLoan al) {
        return loanService.appliedLoan(al);
    }
    
    @GetMapping({"/customer/{csid}", "/customer/{csid}/loans/{loanid}"})
    public ResponseEntity<Object> loanDetails(@PathVariable("csid") String csId, @PathVariable(name = "loanid", required = false) String loanId) {
        System.out.println("cssid :: " + csId + " loanId " + loanId);
        return loanService.loanDetails(csId, loanId);
    }
}
