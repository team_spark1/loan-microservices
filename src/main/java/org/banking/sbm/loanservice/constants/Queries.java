package org.banking.sbm.loanservice.constants;

public interface Queries {
    String ACCOUNTDETAILS = "select account_id,account_balance,account_type, customer_id, cibil_score from account_details where account_id = ?";
    String LOANENRTY= "INSERT INTO loan_details (loan_id, account_id, loan_amount, loan_tenure, loan_exp_date, loan_status, loan_reason, loan_approve_date, customer_id) values (?,?,?,?,?,?,?,?,?)";
    String LOANDETAILS= "select * from loan_details where customer_id = ?";
    String LOANDETAIL= "select * from loan_details where customer_id = ? and loan_id = ?";
    String APPLIEDLOANENTRY="insert into loan_entries(loan_id,amount) values(?,?)";
}
