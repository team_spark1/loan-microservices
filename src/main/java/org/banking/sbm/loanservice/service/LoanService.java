package org.banking.sbm.loanservice.service;

import org.banking.sbm.loanservice.model.AppliedLoan;
import org.banking.sbm.loanservice.model.LoanRequest;
import org.springframework.http.ResponseEntity;

public interface LoanService {

    ResponseEntity<Object> loanDetails(String id, String loan_id);
    ResponseEntity<Object> loanEntry(LoanRequest loanReq);
	ResponseEntity<Object> appliedLoan(AppliedLoan al);
	
	
}
