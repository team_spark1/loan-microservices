package org.banking.sbm.loanservice.service.impl;

import org.banking.sbm.loanservice.model.AccountDetails;
import org.banking.sbm.loanservice.model.AppliedLoan;
import org.banking.sbm.loanservice.model.LoanDetails;
import org.banking.sbm.loanservice.model.LoanRequest;
import org.banking.sbm.loanservice.repository.AccountProcessRepo;
import org.banking.sbm.loanservice.repository.LoanProcessRepo;
import org.banking.sbm.loanservice.service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Service("loanService")
public class LoanServiceImpl implements LoanService {
    @Autowired
    private LoanProcessRepo lpr;

    @Autowired
    private AccountProcessRepo apr;

    @Override
    public ResponseEntity<Object> loanDetails(String csid, String loanId) {
        List<LoanDetails> ld = null;
        if(loanId != null) {
            ld = lpr.loanStatementIndividualEntry(csid, loanId);
        } else {
            ld = lpr.loanStatement(csid);
        }

        if(ld != null) {
            return ResponseEntity.ok(ld);
        } else {
            return ResponseEntity.badRequest().body("Invalid request");
        }
    }

   
	@Override
    public ResponseEntity<Object> loanEntry(LoanRequest lr){
       System.out.println("Dealing with :"+lr.getAccount_id());
    
    	AccountDetails ad = apr.accStatement(lr.getAccount_id());
    	System.out.println("INside loan entry   "+ad.toString());
        if(ad != null)
        {
            if (new BigDecimal(ad.getAccount_balance()).compareTo(new BigDecimal("200000")) > 0 && ad.getCibil_score() > 800) {
               SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
                String loan_id = lr.getAccount_id() + formatter.format(new Date());
                System.out.println(loan_id);
                LocalDate loan_exp_date =  LocalDate.now().plusDays(lr.getTenure());
                lpr.loanEntry(loan_id, lr.getAccount_id(), lr.getLoan_amount(), lr.getTenure(), loan_exp_date.toString(), "Approved", "Sufficient balance and cibil score > 800", new Date(), ad.getCustomer_id());
                
                return ResponseEntity.ok("success");
            } else {
                return ResponseEntity.badRequest().body("Cannot process Loan because of insufficient balance or less cibil score");
            }
        }
        else {
              return ResponseEntity.badRequest().body("Invalid Request");
        }
    }


	@Override
	public ResponseEntity<Object> appliedLoan(AppliedLoan al) {
		System.out.println("Dealing with :"+al.getLoan_id());
	    
    	lpr.appliedLoad(al.getLoan_id(),al.getAmount());
		return ResponseEntity.ok("Success");
	}

	
}
