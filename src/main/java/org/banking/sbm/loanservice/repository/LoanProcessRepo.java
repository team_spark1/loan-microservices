package org.banking.sbm.loanservice.repository;

import org.banking.sbm.loanservice.constants.Queries;
import org.banking.sbm.loanservice.model.LoanDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Repository
public interface LoanProcessRepo extends JpaRepository<LoanDetails, Long> {
    @Query(value = Queries.LOANDETAILS, nativeQuery = true)
    List<LoanDetails> loanStatement (String customer_id);

    @Query(value = Queries.LOANDETAIL, nativeQuery = true)
    List<LoanDetails> loanStatementIndividualEntry (String customer_id, String loan_id);
@Modifying
@Transactional
    @Query(value = Queries.LOANENRTY, nativeQuery = true)
    void loanEntry(String loan_id, String account_id, String loan_amount, int loan_tenure, String loan_exp_date, String loan_status, String reason, Date loan_approve_date, String customerId);

@Modifying
@Transactional
@Query(value = Queries.APPLIEDLOANENTRY, nativeQuery = true)
void appliedLoad(String loan_id, String amount);

}
