package org.banking.sbm.loanservice.repository;

import org.banking.sbm.loanservice.constants.Queries;
import org.banking.sbm.loanservice.model.AccountDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface AccountProcessRepo extends JpaRepository<AccountDetails, Long> {

    @Query(value = Queries.ACCOUNTDETAILS, nativeQuery = true)
    AccountDetails accStatement(String id);

}
