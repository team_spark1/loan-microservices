package org.banking.sbm.loanservice.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "loan_details")
public class LoanDetails {

    @Id
    @Column(name="loan_id")
    private String loan_id;
    @Column(name="account_id")
    private String account_id;
    @Column(name="loan_amount")
    private String loan_amount;
    @Column(name="loan_tenure")
    private int loan_tenure;
    @Column(name="loan_exp_date")
    private String loan_exp_date;
    @Column(name="loan_status")
    private String loan_status;
    @Column(name="loan_reason")
    private String reason;
    @Column(name="loan_approve_date")
    private Date loan_approve_date;
    @Column(name="customer_id")
    private String customer_id;
}
