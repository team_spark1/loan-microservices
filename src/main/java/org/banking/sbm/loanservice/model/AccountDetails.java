package org.banking.sbm.loanservice.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter @Setter
@Entity
@Table(name = "account_details")
public class AccountDetails {
    @Id
    @Column(name="account_id")
    private String account_id;
    @Column(name="account_balance")
    private String account_balance;
    @Column(name="customer_id")
    private String customer_id;
    @Column(name="account_type")
    private String account_type;
    @Column(name="cibil_score")
    private int cibil_score;

}
