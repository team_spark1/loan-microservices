package org.banking.sbm.loanservice.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoanRequest {

    private String account_id;
    private String loan_amount;
    private int tenure;
    
}
