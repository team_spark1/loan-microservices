package org.banking.sbm.loanservice.model;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "loan_entries")
public class AppliedLoan {

	 @Id
	    @Column(name="loan_id")
	    private String loan_id;
	 @Column(name="amount")
	    private String amount;
}
